package lando.katas.kotlin

import org.junit.Assert.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test


internal class RomanTest {

    private lateinit var roman: Roman

    @BeforeEach
    fun setup() {
        roman = Roman()
    }

    @Test
    fun numbers() {
        assertEquals("I", roman.toDecimal(1))
        assertEquals("II", roman.toDecimal(2))
        assertEquals("IV", roman.toDecimal(4))
        assertEquals("V", roman.toDecimal(5))
        assertEquals("VI", roman.toDecimal(6))
        assertEquals("IX", roman.toDecimal(9))
        assertEquals("X", roman.toDecimal(10))
        assertEquals("XV", roman.toDecimal(15))
        assertEquals("XL", roman.toDecimal(40))
        assertEquals("L", roman.toDecimal(50))
        assertEquals("XC", roman.toDecimal(90))
        assertEquals("XCII", roman.toDecimal(92))
        assertEquals("XCIV", roman.toDecimal(94))
        assertEquals("XCVI", roman.toDecimal(96))
        assertEquals("XCVIII", roman.toDecimal(98))
        assertEquals("XCIX", roman.toDecimal(99))
        assertEquals("C", roman.toDecimal(100))
        assertEquals("CI", roman.toDecimal(101))
        assertEquals("CD", roman.toDecimal(400))
        assertEquals("CDXL", roman.toDecimal(440))
        assertEquals("CDL", roman.toDecimal(450))
        assertEquals("D", roman.toDecimal(500))
        assertEquals("CM", roman.toDecimal(900))
        assertEquals("M", roman.toDecimal(1000))
        assertEquals("MMXVIII", roman.toDecimal(2018))
    }
}