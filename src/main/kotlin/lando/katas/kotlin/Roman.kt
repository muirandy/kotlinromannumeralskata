package lando.katas.kotlin

class Roman {
    fun getLetter(number: Int): Pair<Int,String> {

        val values = mapOf<Int, String>(
                1000 to "M",
                500 to "D",
                100 to "C",
                50 to "L",
                10 to "X",
                5 to "V",
                1 to "I"
        )

        val mapping = mapOf<Int, Int>(
                5 to 1,
                10 to 1,
                50 to 10,
                100 to 10,
                500 to 100,
                1000 to 100
        )

        if (values.containsKey(number))
            return Pair(number, values.get(number)!!)

        val slightlyHigher = values.keys
                .filter { it > number }
                .sortedBy { it }
                .firstOrNull()

        if (slightlyHigher != null) {
            var reduceBy = mapping.get(slightlyHigher)
            val reducedNumber = slightlyHigher - (reduceBy ?: 0)
            if (number >= reducedNumber)
                return Pair(reducedNumber, "" + values.get(reduceBy)!! + values.get(slightlyHigher))
        }

        val slightlyLower = values.keys
                .filter { it < number }
                .sortedBy { it }
                .last()

        return Pair(slightlyLower, values.get(slightlyLower)!!)
    }

    fun toDecimal(i: Int): String {
        var result = ""
        var remainder = i

        while (remainder > 0) {
            val nextLetter = getLetter(remainder)
            result += nextLetter.second
            remainder = remainder - nextLetter.first
        }

        return result
    }
}